import React, { useState } from "react"
import { render, fireEvent } from "@testing-library/react"
import { Modal, ModalProps } from "../index"

const ModalTester = (props: ModalProps) => {
  const defaultVisible = props && props.visible || false
  const [visible, setVisible] = useState(defaultVisible)
  return (
    <Modal
      parentSetState = {setVisible || props.parentSetState}
      {...props}
      visible = {visible}
    />
  )
}

describe("test Modal component", () => {
  it('render correctly', () => {
    const parentSetState = jest.fn()
    const wrapper = render(
      <ModalTester visible parentSetState={parentSetState} />
    )
    expect(wrapper).toMatchSnapshot()
  })

  it('onCancel should be called', () => {
    const onCancel = jest.fn()
    const parentSetState = jest.fn()
    const wrapper = render(
      <ModalTester
        visible
        onCancel={onCancel}
        cancelText="Cancel"
        parentSetState={parentSetState}
      />
    )
    const ele = wrapper.getByText("Cancel")

    fireEvent.click(ele)
    expect(onCancel).toHaveBeenCalled()
  })

  it('onOk should be called', () => {
    const onOk = jest.fn()
    const parentSetState = jest.fn()
    const wrapper = render(
      <ModalTester
        visible
        onOk={onOk}
        okText="Cancel"
        parentSetState={parentSetState}
      />
    )
    const ele = wrapper.getByText("Cancel")

    fireEvent.click(ele)
    expect(onOk).toHaveBeenCalled()
  })
})
